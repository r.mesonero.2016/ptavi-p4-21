#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Programa cliente UDP que abre un socket a un servidor
"""

import sys
import socket

# Constantes. Dirección IP del servidor y contenido a enviar
SERVER = 'localhost'


def main():
    # Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
    try:
        IP = sys.argv[1]
        PORT = int(sys.argv[2])
        ARGUMENTOS = sys.argv[3:]
        REGISTRO = ARGUMENTOS[0]
        REGISTRO_NOMBRE = ARGUMENTOS[1]
        EXPIRES_TIME = ARGUMENTOS[2]
    except ValueError:
        sys.exit("puerto and ip must be numbers")
    except IndexError:
        sys.exit("Usage: client.py ip puerto register sip_address"
                 " expires_value")

    try:
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
            my_socket.connect((SERVER, PORT))
            print(REGISTRO.upper() + " sip:" + REGISTRO_NOMBRE +
                  " SIP/2.0")
            print("Expires: " + str(EXPIRES_TIME))
            print()
            # Enviamos al servidor:
            my_socket.send(bytes(REGISTRO.upper() + ' sip:' + REGISTRO_NOMBRE +
                                 ' SIP/2.0 ' + EXPIRES_TIME + '\r\n',
                                 'utf-8') + b'\r\n\r\n')
            # Recibimos del servidor
            data = my_socket.recv(1024)
            print(data.decode('utf-8'))
        print("Cliente terminado.")
    except ConnectionRefusedError:
        print("Error conectando a servidor")


if __name__ == "__main__":
    main()
