#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

import socketserver
import sys
import json
import time
import os

PORT = 6001


class SIPRegisterHandler(socketserver.DatagramRequestHandler):
    """
    Echo server class
    """
    lista_json = []

    def handle(self):
        """
        lee la peticion del cliente y prepara la lista en el formato adecuado
        """
        # Lee del json si existiera:
        if os.path.exists('registered.json'):
            self.json2register()

        string = ""
        # Recoge los datos del cliente:
        for line in self.rfile:
            if line != b'\r\n':
                print(str(self.client_address[0]) + " " +
                      str(self.client_address[1]) + " " +
                      line.decode('utf-8'))
            string = string + line.decode('utf-8')
        ip = self.client_address[0]
        puerto = self.client_address[1]
        lista = string.split(" ")
        # Envia al cliente el OK:
        if lista[0] == "REGISTER":
            self.wfile.write(b"SIP/2.0 200 OK\r\n\r\n")
        # Ordena los datos para crear mi lista:
        registro_nombre = lista[1].strip('sip:')
        expires = lista[3].strip('\r\n')
        tiempo = time.strftime("%Y-%m-%d %H:%M:%S",
                               time.gmtime(int(time.time()) + int(expires)))
        atributos_json = {}
        atributos_json["address"] = ip
        atributos_json["puerto"] = puerto
        atributos_json["expires"] = tiempo
        if expires != "0":
            self.lista_json.append(registro_nombre)
            self.lista_json.append(atributos_json)
        position = 0
        # Para eliminar de la lista si expira el usuario:
        if expires == "0":
            for user in self.lista_json:
                if user == registro_nombre:
                    del self.lista_json[position]
                    del self.lista_json[position]['address']
                    del self.lista_json[position]['puerto']
                    del self.lista_json[position]['expires']
                position = position + 1
        position = 0
        for borrar in self.lista_json:
            if borrar == {}:
                del self.lista_json[position]
            position = position + 1

        self.register2json()

    def register2json(self):
        """
        crea el json con el formato que tiene la lista
        """
        with open('registered.json', 'w') as jsonfile:
            json.dump(self.lista_json, jsonfile, indent=4)

    def json2register(self):
        """
        abre el json y los datos los carga en la lista
        """
        try:
            with open('registered.json', 'r') as jsonfile:
                self.lista_json = json.load(jsonfile)
        except():
            pass


def main():
    # Listens at port PORT (my address)
    # and calls the EchoHandler class to manage the request
    try:
        serv = socketserver.UDPServer(('', PORT), SIPRegisterHandler)
        print(f"Server listening in port {PORT}")
    except OSError as e:
        sys.exit(f"Error empezando a escuchar: {e.args[1]}.")

    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")
        sys.exit(0)


if __name__ == "__main__":
    main()
